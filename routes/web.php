<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('discussions', 'DiscussionsController')->middleware('auth');
Route::get('mydiscussions', 'DiscussionsController@myDiscussions')->name('discussions.mydiscussions')->middleware('auth');
Route::get('discussions/delete/{id}', 'DiscussionsController@destroy')->name('discussion.delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
