@extends('layouts.app')

@section('title', 'Create discussion')

@section('content')
        <h1>Create discussion</h1>
        <form method = "post" action = "{{action('DiscussionsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "name">Discussion name</label>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group">
            <label for = "date">Discussion date</label>
            <input type = "date" class="form-control" name = "date">
        </div> 
        <div class="form-group">
            <label for = "hall">hall_id</label>
            <input type = "text" class="form-control" name = "hall_id">
            </div> 
            <div class="form-group">
            <label for = "judge">judge_id</label>
            <input type = "text" class="form-control" name = "judge_id">
            </div> 
            <div class="form-group">
            <label for = "writer">writer_id</label>
            <input type = "text" class="form-control" name = "writer">
            </div> 
            <div class="form-group">
            <label for = "type">type_id</label>
            <input type = "text" class="form-control" name = "type">
            </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create discussion">
        </div>                       
        </form>    
@endsection
