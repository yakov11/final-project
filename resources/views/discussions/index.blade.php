@extends('layouts.app')
@section('title', 'Discussions')
@section('content')

<div><a href =  "{{url('/discussions/create')}}"> Add new discussion</a></div>
<h1>List of discussions</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Hall name</th><th>Judge name</th><th>Writer name</th><th>discussion type</th>
    </tr>
    
    @foreach($discussions as $discussion)
        <tr>
            <td>{{$discussion->id}}</td>
            <td>{{$discussion->date}}</td>
            <td>{{$discussion->hall->name}}</td>
            <td>{{$discussion->judge->name}}</td>
            <td>{{$discussion->writer->name}}</td>
            <td>{{$discussion->type->name}}</td>
            

           
            <td>
                <a href = "{{route('discussions.edit',$discussion->id)}}">Edit</a>
            </td> 
            <td>
                    <a href = "{{route('discussion.delete',$discussion->id)}}">Delete</a>
            </td>                                                               
        </tr>
    @endforeach
</table>

@endsection
