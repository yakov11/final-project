<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model
{
    protected $fillable = ['date', 'hall_id', 'judge_id', 'writer_id', 'type_id'];

    public function judge(){
        return $this->belongsTo('App\Judge');
    }
    public function writer(){
        return $this->belongsTo('App\Writer');
    }
    public function hall(){
        return $this->belongsTo('App\Hall');
    }
    public function type(){
        return $this->belongsTo('App\Type');
    }
    
    
}
