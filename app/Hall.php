<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    public function discussion(){
        return $this->hasMany('App\Discussion');
    }
}

